<?php
if ( ! defined( 'ABSPATH' ) ) {
	die( 'You are not allowed to call this page directly.' );
}

/**
 * Displays the Settings Page.
 *
 * @return void
 */

function ffdl_display_page() {
	$forms         = ffdl_get_forms();
	$ffdl_settings = get_option( 'ffdl_settings' );

	if ( ! $forms ) {
		die( "Forms doesn't exists" );
	}

	?>
	<div class="ffdl-settings-div" >
	<div class="jumbotron text-center">
	<h1><?php esc_html_e( 'Formidable Forms Date Limiter', 'ff-dl' ); ?></h1>

	</div>
	<div class="container" >
		<form  id="ffdl-commission-settings-form" style="min-width:100%;" >
		<div id="ffdl-message" class="text-center text-white"></div>

	<input type="hidden" name='action' value='process_ffdl_settings_form'>
			<?php wp_nonce_field( 'ffdl_nonce_verify' ); ?>
		<div class="row">
		<?php
		foreach ( $forms as $form => $fields ) {
			?>
<div class="card col-sm-4" style="min-width:48%;margin:1%;justify-content:space-between;">

<div class="card-body" style="margin-top:2%;">
<div id="ffdl-settings-inner-body" >
<div class="row text-center">
<div  class="col-sm-12 " ><h1><?php echo esc_html( 'Form-' . $form ); ?></h1> </h1>
</div>

<div  class="col-sm-6 " ><h2><?php esc_html_e( 'Fields', 'ff-dl' ); ?> </h2>
</div>
<div  class="col-sm-6 "> <h2><?php esc_html_e( 'Value', 'ff-dl' ); ?> </h2> </div>
</div>
			<?php
			// Looping and creating HTML.
			foreach ( $fields as $field ) {
				$value = isset( $ffdl_settings[ $form ][ $field ] ) ? $ffdl_settings[ $form ][ $field ] : 10;
				// $value = 10;
				?>
<div class="row text-center form-group">
<div  class="col-sm-6 " ><label for="id-<?php echo esc_html( $form . '-' . $field ); ?>"></label> <h2><?php echo esc_html( 'Date-Field-' . $field ); ?> </h2>
</div>
<div  class="col-sm-6 "> <input style="max-width:95%;" type="number" class="form-control" id="id-<?php echo esc_html( $form . '-' . $field ); ?>" value="<?php echo esc_html( $value ); ?>" name="ffdl_settings[<?php echo esc_html( $form ); ?>][<?php echo esc_html( $field ); ?>]"> </div>
</div>
				<?php
			}

			?>

		</div>
</div>

</div>	
			<?php
		}
		?>
</div>
<div class="row text-center justify-content-center" style="margin-top:2%">
		<button type="submit" class="btn-lg bg-success text-white" ><?php esc_html_e( 'Submit', 'ff-dl' ); ?></button>
		</div>
		</form>
</div>
</div>
			<?php
}
