=== Formidable Forms - Contact Form, Survey, Quiz, Calculator & Custom Form Builder ===
Plugin Name: Formidable Forms Date Limiter
Contributors: Rehan K

== Overall Process == 

The Plugin allows you to limit how many times a date can be choosed in Formidable Forms.

It has a settings page which allows you to set the numeric limiter for each Form and each field. That data is stored in wp_options table in the option ffdl_settings. 

The Date increments are saved in a seperate option field for each form, with the following syntax:- 'ffdl_form_' . $form_id. saved as an associative array, with the fields as keys, and the values are again an associative array with the dates as keys and their values as increments. The Styling is completed by a customized version of Bootstrap.


