<?php
if ( ! defined( 'ABSPATH' ) ) {
	die( 'You are not allowed to call this page directly.' );
}

/**
 * Gets all the date fields from formidable forms.
 *
 * @param [type] $form_id
 * @return void
 */
function get_date_fields( $form_id ) {
	global $wpdb;

	$arr = array();

	$query_results = $wpdb->get_results( $wpdb->prepare( "SELECT id FROM {$wpdb->prefix}frm_fields WHERE `type`='date' AND `form_id`=%d", $form_id ), ARRAY_A ); // db call ok; no-cache ok.

	return $query_results;
}
