<?php

if ( ! defined( 'ABSPATH' ) ) {
	die( 'You are not allowed to call this page directly.' );
}

/**
 * Gets all the Forms.
 *
 * @return void
 */
function ffdl_get_forms() {

	global $wpdb;

	$arr = array();

	$query_results = $wpdb->get_results( "SELECT id, form_id FROM wp_frm_fields WHERE `type`='date'", ARRAY_A ); // db call ok; no-cache ok.

	foreach ( $query_results as $result ) {

		$arr[ $result['form_id'] ][] = $result['id'];

	}

	return $arr;

}
