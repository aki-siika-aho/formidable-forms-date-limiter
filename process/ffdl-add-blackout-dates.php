<?php
if ( ! defined( 'ABSPATH' ) ) {
	die( 'You are not allowed to call this page directly.' );
}

/**
 * Add blackout dates.
 *
 * @return arr constraints.
 */
function ffdl_add_blackout_dates( $constraints, $field ) {

	// Conditional Checks.
	if ( ! get_option( 'ffdl_form_' . $field->form_id ) || ! get_option( 'ffdl_settings' ) ) {

		return $constraints;

	}

	$ffdl_form = get_option( 'ffdl_form_' . $field->form_id );

	$ffdl_settings = get_option( 'ffdl_settings' );

	if ( ! isset( $ffdl_form[ $field->id ] ) ) {

		return $constraints;

	}
	$limit = $ffdl_settings[ $field->form_id ][ $field->id ];

	$blackout_dates = array_filter(
		$ffdl_form[ $field->id ],
		function( $date ) use ( $limit ) {
			return $date >= $limit;
		}
	);

	$blackout_dates = array_flip( $blackout_dates );

	$blackout_dates = array_values( $blackout_dates );

	foreach ( $blackout_dates as $key => $value ) {

		$parts                  = explode( '/', $value );
		$blackout_dates[ $key ] = $parts[2] . '-' . $parts[0] . '-' . $parts[1];
	}

	$constraints = array_merge( $constraints, $blackout_dates );

	return $constraints;

}
