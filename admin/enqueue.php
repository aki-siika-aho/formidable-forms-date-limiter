<?php

if ( ! defined( 'ABSPATH' ) ) {
	die( 'You are not allowed to call this page directly.' );
}

/**
 * Enqueues Scripts and Styles on Admin Side.
 *
 * @return void
 */
function ffdl_enqueue() {

	wp_register_style( 'ffdl_bootstrap', plugins_url( 'assets/css/bootstrap.css', FFDL_PATH ), array(), false );
	wp_register_style( 'ffdl_custom', plugins_url( 'assets/css/custom.css', FFDL_PATH ), array(), false );

	// Enqueue Styles.
	wp_enqueue_style( 'ffdl_bootstrap' );
	wp_enqueue_style( 'ffdl_custom' );

	// Register Scripts.

	wp_register_script( 'ffdl_main', plugins_url( 'assets/js/main.js', FFDL_PATH ), array(), false, true );

	// Enqueue Scripts.

	wp_enqueue_script( 'ffdl_main' );

	wp_localize_script( 'ffdl_main', 'ffdl_obj', array( 'ajax_url' => admin_url( 'admin-ajax.php' ) ) );
}
