<?php

if ( ! defined( 'ABSPATH' ) ) {
	die( 'You are not allowed to call this page directly.' );
}

/**
 * Registers the Settings Page.
 *
 * @return void
 */
function ffdl_register_page() {
	add_menu_page(
		__( 'Formidable Forms Date Limiter Settings', 'ff-dl' ),
		'Formidable Forms Date Limiter Settings',
		'manage_options',
		'ff-date-limiter-settings',
		'ffdl_display_page',
	);

}
