<?php
if ( ! defined( 'ABSPATH' ) ) {
	die( 'You are not allowed to call this page directly.' );
}

/**
 * Adds date increments at the appropriate location.
 *
 * @param [type] $entry_id
 * @param [type] $form_id
 * @return void
 */
function ffdl_date_increment( $entry_id, $form_id ) {

	if ( ! get_option( 'ffdl_form_' . $form_id ) ) {

		update_option( 'ffdl_form_' . $form_id, array() );
	}

	$ffdl_form = get_option( 'ffdl_form_' . $form_id );

	$date_fields = get_date_fields( $form_id );

	foreach ( $date_fields as $field ) {

		$field_id    = $field['id'];
		$field_value = $_POST['item_meta'][ intval( $field_id ) ];

		if ( isset( $_POST['item_meta'][ intval( $field_id ) ] ) && ! empty( $_POST['item_meta'][ intval( $field_id ) ] ) ) {

			if ( ! isset( $ffdl_form[ $field_id ] ) ) {

				$ffdl_form[ $field_id ] = array();

			}

			if ( ! isset( $ffdl_form[ $field_id ][ $field_value ] ) ) {

				$ffdl_form[ $field_id ][ $field_value ] = 1;

			} else {
				$ffdl_form[ $field_id ][ $field_value ] += 1;
			}
		}
	}

	update_option( 'ffdl_form_' . $form_id, $ffdl_form );
}
