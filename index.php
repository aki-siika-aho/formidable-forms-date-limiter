<?php if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

/*
Plugin Name: Formidable Forms Date Limiter.
Plugin URI: https://gitlab.com/aki-siika-aho/formidable-forms-date-limiter
Description: A custom plugin that will allow the user to define a number of times a date field can be choosed.
Version: 1.0
Author: Rehan Khan
Author URI: https://www.codeable.io/developers/aman-khan/
textdomain: ff-dl
*/

define( 'FFDL_PATH', __FILE__ );

// Includes.

require 'activation.php';
require 'admin/register-ffdl-page.php';
require 'admin/display-ffdl-page.php';
require 'admin/enqueue.php';
require 'utility/get-forms.php';
require 'process/process-ffdl-settings-form.php';
require 'process/ffdl-date-increment.php';
require 'utility/get-date-fields.php';
require 'process/ffdl-add-blackout-dates.php';


if ( in_array( 'formidable/formidable.php', get_option( 'active_plugins' ), true ) ) {

	// Actions.
	register_activation_hook( FFDL_PATH, 'ffdl_activate_plugin' );
	add_action( 'admin_menu', 'ffdl_register_page' );
	add_action( 'admin_enqueue_scripts', 'ffdl_enqueue' );
	add_action( 'wp_ajax_process_ffdl_settings_form', 'process_ffdl_settings_form' );
	add_action( 'frm_after_create_entry', 'ffdl_date_increment', 30, 2 );

	// Filter.
	add_filter( 'frm_dates_disabled', 'ffdl_add_blackout_dates', 10, 2 );


	// Shortcodes.


} else {
	add_action( 'admin_notices', 'core_plugins_required' );

};
