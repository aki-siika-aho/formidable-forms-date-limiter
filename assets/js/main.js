/**
 * Main JS file for sending Form data.
 */
(function($) {
 
    $('#ffdl-commission-settings-form').on('submit', function (e) {
        e.preventDefault();
        $('#ffdl-message').html('')
      
        //Serializes the complete form data.
        var form_data = $(this).serialize();

        //Sending Form data.
        $.post(ffdl_obj.ajax_url, form_data, function(response){

        if(response.status == 2)
        {
            $('#ffdl-message').html('<div class="bg-success text-white" style="padding:1%;"><h1 style="color:white;" >Settings Updated Successfully</h1></div>')
        }
        else 
        {
            $('#ffdl-message').html('<div class="bg-danger text-white" style="padding:1%;"><h1 style="color:white;" >An Error has occured</h1></div>')
      
        }

        })

      })
 
    })(jQuery);