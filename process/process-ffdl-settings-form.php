<?php
if ( ! defined( 'ABSPATH' ) ) {
	die( 'You are not allowed to call this page directly.' );
}

/**
 * Processes Settings Form and Saves Values.
 *
 * @return void
 */
function process_ffdl_settings_form() {

	check_admin_referer( 'ffdl_nonce_verify' );

	$response = array( 'status' => 1 );

	if ( ! current_user_can( 'manage_options' ) ) {
		wp_send_json( $response );
	}

	if ( ! isset( $_POST['ffdl_settings'] ) ) {
		wp_send_json( $response );
	}

	$ffdl_settings = wp_unslash( $_POST['ffdl_settings'] );

	update_option( 'ffdl_settings', $ffdl_settings );

	$response = array( 'status' => 2 );

	wp_send_json( $response );
}
