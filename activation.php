<?php
if ( ! defined( 'ABSPATH' ) ) {
	die( 'You are not allowed to call this page directly.' );
}

/**
 * Runs when the plugin is activated. Adds initial data to the settings.
 *
 * @return void
 */
function ffdl_activate_plugin() {

	if ( ! get_option( 'ffdl_form_settings' ) ) {

		$forms = ffdl_get_forms();

		$ffdl_settings = array();
		foreach ( $forms as $form => $fields ) {
			foreach ( $fields as $field ) {
				$ffdl_settings[ $form ][ $field ] = 10;
			}
		}

		update_option( 'ffdl_form_settings', $ffdl_settings );
	}
}
